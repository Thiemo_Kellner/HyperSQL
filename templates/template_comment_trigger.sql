/** Short_description_to_the_next_linebreak_dot_semicolon.
  *
  * Here_follows_the_long_description__use_of_underscore_instead_of
  * spaces_is_for_easier_replacement__the_first_line_starting_with
  * the_at_sign_terminates_the_description_part.
  *
  * @trigger Object_name_to_be_replaced_by_the_actual_name
  *
  * @author Whatever_description_text_or_something_you_want_here
  * @author Anotherone
  *
  * @bug Description_of_things_that_need_fixing_sort_of_higher_prio_todo.
  * @bug Anotherone.
  *
  * @copyright Copyright_string
  * @copyright Anotherone
  *
  * @deprecated Describe_what_to_instead.
  *
  * @example Give_an_example_for_its_usage
  * @example Anotherone
  *
  * @ignore Remove_this_description_for_this_tag_is_an_instruction_for_hypersql_to_ignore_this_object_when_generating_the_reference
  *
  * @ignorevalidation Remove_this_description_for_this_tag_is_an_instruction_for_hypersql_to_ignore_this_object_when_validating_the_doc
  *
  * @info Place_some_additional_information_for_the_developers.
  * @info Anotherone.
  *
  * @private Remove_this_description_for_this_tag_is_an_instruction_for_hypersql_to_not_place_this_object_in_the_package_specs
  *
  * @see Pointer_to_other_sources
  * @see Anotherone
  *
  * @since Place_a_point_in_time_or_version_numer__it_will_be_translated_to_Available_since
  *
  * @testcase Replace_with_the_testcase_block__not_straight_forward_as_something_internal_I_believe
  * @testcase Anotherone
  *
  * @throws Exception_name_to_be_replace_by_the_actual_exception_name
  *           If_you_want_to_describe_the_exception_do_it_here.
  * @throws Anotherone
  *           Anotherone.
  *
  * @ticket Replace_with_the_ticket_id Short_description_of_the_ticket.
  * @ticket Anotherone Anotherone.
  *
  * @todo Description_of_things_that_need_to_be_done.
  * @todo Anotherone.
  *
  * @used Place_object_name_that_uses_this_and_that_cannot_be_resolved_by_hypersql_for_lack_of_code
  * @used Anotherone
  *
  * @uses Place_object_name_that_uses_this_and_that_cannot_be_resolved_by_hypersql_for_lack_of_code
  * @uses Anotherone
  *
  * @version Place_version_information_of_this_object_eg_subversion_keyword_version
  * @version Anotherone
  *
  * @verbatim Details_that_need_preformatting_like_code_samples
  * @verbatim Anotherone
  *
  * @webpage Place_url_where_to_find_more_details_on_this_object
  * @webpage Anotherone
  *
  * @wiki Replace_by_page_name Replace_with_some_text_describing_the_wiki_project_application
  *
  */
